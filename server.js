'use strict';

// INITIALIZATIONS
const path    = require('path');
const http    = require('http');
const fs      = require('fs');

const express = require('express');
const app     = express();
const server  = http.createServer(app); 
const port    = process.env.PORT || 3000;
const io      = require('socket.io')(server);
const axios   = require('axios');
const shell   = require('shelljs');
const glob    = require('glob');

// SOCKETS
let fileCounter = 0;
const filePath = 'public/photos/';

glob("public/photos/*.jpg", (er, files) => {
    fileCounter = files.length;
});

io.on('connection', (socket) => {
    console.log('Client connected');

    socket.on('makePhoto', () => {
        fileCounter++;
        shell.exec(`gphoto2 --capture-image-and-download --filename ${filePath}photo_${fileCounter}.jpg`);
        socket.emit('savedPhoto', {n: fileCounter})
    })
});

// PUBLIC FILES
app.use(express.static('public'));
app.use('/scripts', express.static('node_modules'));
app.use('/php', express.static('scripts'));

// ROUTES
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + "/views/frontpage.html"));
});

// app.get('/gallery', (req, res) => {
//     res.sendFile(path.join(__dirname + "/views/gallery.html"));
// });

// START SERVER
server.listen(port, () => {
    console.log(`Running on port ${port}`)
});