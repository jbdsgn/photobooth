// VUE
const app = new Vue({
    el: '#app',
    data: {
        countdown_val: 6,
        pic: ''
    },
    methods: {
        shoot: function() {
            $('.intro').addClass('fade-out');
            $('.countdown').addClass('fade-in');

            setInterval(() => {
                if(app.countdown_val > 0) {
                    app.countdown_val--
                }
                if(app.countdown_val == 0) {
                    app.countdown_val = "Cheeese!";

                    setTimeout(() => {
                        socket.emit('makePhoto');
                    }, 1000);
                }
            }, 1000)
        }
    }
});

socket.on('savedPhoto', (data) => {
    setTimeout(() => {
        app.pic = `./photos/photo_${data.n}.jpg`;
        $('.preview-img').fadeIn();

        setTimeout(() => {
            location.reload();
        }, 7000);
    }, 1000);
});